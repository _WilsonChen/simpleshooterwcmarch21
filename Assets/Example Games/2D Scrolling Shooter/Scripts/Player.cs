﻿using UnityEngine;
using System.Collections;

//This script manages the player object
public class Player : Spaceship
{	
	void Update ()
	{
		Move ();
		shotDelay = 1f;
		if (Input.GetKey ("space")) {
			shotDelay = 0.1f;
			Shoot();
		}
	}
	
	void Move ()
	{
		Vector2 mousePosition = Camera.main.ScreenToWorldPoint(new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 0));
		//Get the player's current position
		float potentialX = mousePosition.x;
		float potentialY = mousePosition.y;
		if (potentialX <= -3.4)
			potentialX = -3.4f;
		else if ( potentialX >= 3.4)
			potentialX = 3.4f;
		if (potentialY <= -2.4) 
			potentialY = -2.4f;
		else if(potentialY >= 2.4)
			potentialY = 2.4f;
        transform.position = new Vector2 (potentialX, potentialY);
	}

	void OnTriggerEnter2D (Collider2D c)
	{
		//Get the layer of the collided object
		string layerName = LayerMask.LayerToName(c.gameObject.layer);
		//If the player hit an enemy bullet or ship...
		if( layerName == "Bullet (Enemy)" || layerName == "Enemy")
		{
			//...and the object was a bullet...
			if(layerName == "Bullet (Enemy)" )
				//...return the bullet to the pool...
			    ObjectPool.current.PoolObject(c.gameObject) ;
			//...otherwise...
			else
				//...deactivate the enemy ship
				c.gameObject.SetActive(false);

			//Tell the manager that we crashed
			Manager.current.GameOver();
			//Trigger an explosion
			Explode();
			//Deactivate the player
			gameObject.SetActive(false);
		}
	}
	IEnumerator Shoot ()
	{
		//Loop indefinitely
		while(true)
		{
			//If there is an acompanying audio, play it
			if (GetComponent<AudioSource>())
				GetComponent<AudioSource>().Play ();
			//Loop through the fire points
			for(int i = 0; i < shotPositions.Length; i++)
			{
				//Get a pooled bullet
				GameObject obj = ObjectPool.current.GetObject(bullet);
				//Set its position and rotation
				obj.transform.position = shotPositions[i].position;
				obj.transform.rotation = shotPositions[i].rotation;
				//Activate it
				obj.SetActive(true);
			}
			//Wait for it to be time to fire another shot
			yield return new WaitForSeconds(shotDelay);
		}
	}
}